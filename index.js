/**
 * Bài tập 1
 * Input:
 *
 * Các bước thực hiện:
 *    + Khai báo và gán biến sum = 0
 *    + Dùng vòng lặp for(var i = 0; sum < 10000; i++), với mỗi vòng lặp thực hiện lệnh:
 *          Gán biến sum += i
 *    +In kết quả i-1 ra giao diện web
 *
 * Output: số nguyên dương nhỏ nhất
 */

function baiTap1() {
  var sum = 0;
  for (var i = 0; sum < 10000; i++) {
    sum += i;
  }
  document.getElementById(
    "result-b1"
  ).innerHTML = `Số nguyên dương nhỏ nhất: ${--i}`;
}

/**
 * Bài tập 2
 * Input: nhập số nguyên x và n
 *
 * Các bước thực hiện:
 *    + Khai báo và gán biến soX, soN theo giá trị của HTML element
 *    + Khai báo và gán biến sum = 0
 *    + Dùng vòng lặp for(var i = 1; i <= soN; i++), với mỗi vòng lặp thực hiện lệnh:
 *          Gán biến sum += Math.pow(soX,i)
 *    + In kết quả ra giao diện web
 *
 * Output: tổng S(n) = x + x^2 + x^3 + … + x^n
 */

function baiTap2() {
  var soX = document.getElementById("so-x").value * 1;
  var soN = document.getElementById("so-n").value * 1;
  var sum = 0;

  for (var i = 1; i <= soN; i++) {
    sum += Math.pow(soX, i);
  }
  document.getElementById("result-b2").innerHTML = `Kết quả tổng: ${sum}`;
}

/**
 * Bài tập 3
 * Input: nhập số nguyên n
 *
 * Các bước thực hiện:
 *    + Khai báo và gán biến n theo giá trị của HTML element
 *    + Khai báo và gán biến result = 1
 *    + Dùng vòng lặp for(var i = 1; i <= n; i++), với mỗi vòng lặp thực hiện lệnh:
 *          Gán biến result *= i
 *    +In kết quả ra giao diện web
 *
 * Output: kết quả giai thừa
 */

function baiTap3() {
  var n = document.getElementById("number").value * 1;
  var result = 1;
  for (var i = 1; i <= n; i++) {
    result *= i;
  }
  document.getElementById(
    "result-b3"
  ).innerHTML = `Kết quả giai thừa: ${result}`;
}

/**
 * Bài tập 3
 * Input:
 *
 * Các bước thực hiện:
 *    + DOM đến 10 thẻ div có class "result-block" đã tạo ở file HTML và gán tên biến divList cho nó
 *    + Dùng vòng lặp for(var i = 1; i <= divList.length; i++), với mỗi vòng lặp thực hiện lệnh:
 *        Nếu i chẵn thì phần tử của divList tại vị trí i có màu xanh và in chữ "Div lẻ" (vì index của divList bắt đầu từ 0)
 *        Ngược lại thì phần tử của divList tại vị trí i có màu đỏ và in chữ "Div chẵn"
 *
 * Output: hiển thị thẻ kèm màu theo thứ tự chẵn lẻ
 */

function baiTap4() {
  var divList = document.querySelectorAll(".result4 .result-block");
  for (var i = 0; i < divList.length; i++) {
    if (i % 2 == 0) {
      divList[i].style.background = "#6495ed";
      divList[i].innerText = "Div lẻ";
    } else {
      divList[i].style.background = "#ff4040";
      divList[i].innerText = "Div chẵn";
    }
  }
  var divResult = (document.querySelector(".result4").style.padding =
    "0 20px 5px 20px");
}
